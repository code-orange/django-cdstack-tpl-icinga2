#!/bin/bash
set -e
set -x

mysql -h {{ monitoring_icinga2_web2_host }} -u root -p < /root/monstack-icinga2-db-helper/mysql_create_dbs_and_users.sql

mysql -h {{ monitoring_icinga2_web2_host }} -u {{ monitoring_icinga2_web2_user }} -p{{ monitoring_icinga2_web2_passwd }} {{ monitoring_icinga2_web2_db }} < /usr/share/icingaweb2/etc/schema/mysql.schema.sql
mysql -h {{ monitoring_icinga2_ido_host }} -u {{ monitoring_icinga2_ido_user }} -p{{ monitoring_icinga2_ido_passwd }} {{ monitoring_icinga2_ido_db }} < /usr/share/icinga2-ido-mysql/schema/mysql.sql
mysql -h {{ monitoring_icinga2_director_host }} -u {{ monitoring_icinga2_director_user }} -p{{ monitoring_icinga2_director_passwd }} {{ monitoring_icinga2_director_db }} < /usr/share/icingaweb2/ext-modules/director/schema/mysql.sql
mysql -h {{ monitoring_icinga2_reporting_host }} -u {{ monitoring_icinga2_reporting_user }} -p{{ monitoring_icinga2_reporting_passwd }} {{ monitoring_icinga2_reporting_db }} < /usr/share/icingaweb2/ext-modules/reporting/schema/mysql.sql
mysql -h {{ monitoring_icinga2_x509_host }} -u {{ monitoring_icinga2_x509_user }} -p{{ monitoring_icinga2_x509_passwd }} {{ monitoring_icinga2_x509_db }} < /usr/share/icingaweb2/ext-modules/x509/etc/schema/mysql.schema.sql

mysql -h {{ monitoring_icinga2_web2_host }} -u {{ monitoring_icinga2_web2_user }} -p{{ monitoring_icinga2_web2_passwd }} {{ monitoring_icinga2_web2_db }} < /root/monstack-icinga2-db-helper/mysql_add_icingaweb2_admin.sql
