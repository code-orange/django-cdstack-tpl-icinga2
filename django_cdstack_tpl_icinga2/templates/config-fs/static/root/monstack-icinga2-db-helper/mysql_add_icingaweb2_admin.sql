/*
The default admin account password is "admin"
*/
USE {{ monitoring_icinga2_web2_db }};
INSERT INTO icingaweb_user (name, active, password_hash) VALUES ('admin', 1, '$1$TnWgXzzc$EC1kVIEOXCX6ZcFsxnsAQ1');
