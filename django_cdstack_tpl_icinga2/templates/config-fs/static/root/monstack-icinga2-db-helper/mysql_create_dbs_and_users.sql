DROP DATABASE IF EXISTS {{ monitoring_icinga2_web2_db }};
DROP DATABASE IF EXISTS {{ monitoring_icinga2_ido_db }};
DROP DATABASE IF EXISTS {{ monitoring_icinga2_director_db }};
DROP DATABASE IF EXISTS {{ monitoring_icinga2_reporting_db }};
DROP DATABASE IF EXISTS {{ monitoring_icinga2_x509_db }};

CREATE DATABASE {{ monitoring_icinga2_web2_db }};
CREATE DATABASE {{ monitoring_icinga2_ido_db }};
CREATE DATABASE {{ monitoring_icinga2_director_db }};
CREATE DATABASE {{ monitoring_icinga2_reporting_db }};
CREATE DATABASE {{ monitoring_icinga2_x509_db }};

GRANT ALL ON {{ monitoring_icinga2_web2_db }}.* TO '{{ monitoring_icinga2_web2_user }}'@'%' IDENTIFIED BY '{{ monitoring_icinga2_web2_passwd }}';
GRANT ALL ON {{ monitoring_icinga2_ido_db }}.* TO '{{ monitoring_icinga2_ido_user }}'@'%' IDENTIFIED BY '{{ monitoring_icinga2_ido_passwd }}';
GRANT ALL ON {{ monitoring_icinga2_director_db }}.* TO '{{ monitoring_icinga2_director_user }}'@'%' IDENTIFIED BY '{{ monitoring_icinga2_director_passwd }}';
GRANT ALL ON {{ monitoring_icinga2_reporting_db }}.* TO '{{ monitoring_icinga2_reporting_user }}'@'%' IDENTIFIED BY '{{ monitoring_icinga2_reporting_passwd }}';
GRANT ALL ON {{ monitoring_icinga2_x509_db }}.* TO '{{ monitoring_icinga2_x509_user }}'@'%' IDENTIFIED BY '{{ monitoring_icinga2_x509_passwd }}';

FLUSH PRIVILEGES;
